package case_2;

import java.awt.Container;
import javax.swing.*;

public class Case_2 {

    public static void main(String[] args) {
        
        
        JFrame guiImageDemo = new GUI_Image_Demo();
        //Set title
        guiImageDemo.setTitle("Image Demo");
        // What to do when the window closes:
        guiImageDemo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // Size of the window, in pixels
        guiImageDemo.setSize(1500, 800);
        // Make the window "visible"
        guiImageDemo.setVisible(true);
        
    } // End main
    
} // End class Case_2
