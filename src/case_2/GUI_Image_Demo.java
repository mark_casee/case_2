package case_2;

import static com.sun.java.accessibility.util.AWTEventMonitor.addItemListener;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import javax.swing.*;
import java.io.*;
import javax.imageio.ImageIO;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.geom.Line2D;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class GUI_Image_Demo extends JFrame {
    
    // Browse Button
    private final JButton BrowseButton;
    private final JLabel label;
    BufferedImage getImg;
    
    // Eastern Panel
    private final JPanel rightHandSideP;
    
    JPanel card3 = new JPanel();
    
    // Combo Box Panel
    private final JPanel comboBoxPanel;
    private final JPanel cards;
    private final static String FilterPanel = "Filter Options for File";
    private final static String DrawPanel = "Draw on File";
    private final static String urlPanel = "Saved Files";
    private final static int GAP = 80; // Spaces filter buttons apart
   
    // Filter Buttons
    private final JButton grayScale = new JButton("Gray Scale File");
    private final JButton inverted = new JButton("Invert Colors on File");
    private final JButton mirrored = new JButton("Mirror File"); 
    private final JButton blur = new JButton("Blur File");
    private final JButton draw = new JButton("Draw");
    
    private final JButton displayFile = new JButton("Display File");
    
    // Save Button
    private final JButton saveButton = new JButton("Save Files");
    private File selFile1;
    private File output = new File("Output.png");
    private File selFile2;
    private String path1;
    private String path2 = System.getProperty("user.home") + File.separator + "Desktop\\outFile.png" ;
   
    public GUI_Image_Demo() {

        // Set default layout
        this.setLayout(new BorderLayout());
        
        // Create button that user clicks to open files
        BrowseButton = new JButton("Upload Files");
        BrowseButton.setPreferredSize(new Dimension(50, 40));
        this.add(BrowseButton, BorderLayout.SOUTH); 
        
        // Add JLabel
        label = new JLabel();
        label.setPreferredSize(new Dimension(20,20));
        add(label);
        
        BrowseButton.setMnemonic('U'); // Hot key
        BrowseButton.setToolTipText("Click or press ALT-U");
        // Browse Files Button ActionListener
        BrowseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser getFile = new JFileChooser();
                getFile.setCurrentDirectory(new File(System.getProperty("user.home")));
                // Filter files
                FileNameExtensionFilter filter1 = new FileNameExtensionFilter("*.Images", "jpg", "png");
                getFile.addChoosableFileFilter(filter1);
                int res = getFile.showSaveDialog(null);
                if(res == JFileChooser.APPROVE_OPTION) {
                    selFile1 = getFile.getSelectedFile();
                    path1 = selFile1.getAbsolutePath();
                    label.setIcon(resize(path1));
                    System.out.println("1st selFile1 = " + selFile1);
                    try {
                        getImg = ImageIO.read(selFile1);
                    } catch (IOException ex) {
                        System.out.println(ex);
                    } // End try-catch
                    
                } // End if
            } // End actionPerformer
        }); // End ActionListener
        
        saveButton.setMnemonic('S'); // Hot key
        saveButton.setToolTipText("Click or press ALT-S");
        // Save Files Button ActionListener
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (selFile1 == null) {
                    JOptionPane.showMessageDialog(saveButton, "File is null, please upload a png file to save.");
                    return;
                }
                JFileChooser getFile = new JFileChooser();
                getFile.setCurrentDirectory(new File(System.getProperty("user.home")));
                // Filter files
                FileNameExtensionFilter filter1 = new FileNameExtensionFilter("*.Images", "jpg",
                        "png");
                getFile.addChoosableFileFilter(filter1);
                int res = getFile.showSaveDialog(null);
                if(res == JFileChooser.APPROVE_OPTION) {
                    selFile2 = getFile.getSelectedFile();
                    path1 = selFile2.getAbsolutePath();
                    System.out.println("1st selFile1 = " + selFile2); 
                    
                    try {
                        ImageIO.write(getImg, "png", selFile2);
                    } catch (IOException ex) {
                        System.out.println(ex);
                    } // End try-catch
                    
                } // End if
            } // End actionPerformer
        }); // End ActionListener
        
        // Draw button ActionListener
        draw.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            if (selFile1 == null) {
                JOptionPane.showMessageDialog(saveButton, "File is null, please upload a png file to save.");
            return;
            }
            
            File f = selFile1;
            
            // Read image
            try{
                getImg = ImageIO.read(f);
            } catch(IOException ef){
                System.out.println(ef);
            } // End try-catch
            
                MouseListener ml = new MouseListener();

            try {
                ImageIO.write(getImg, "png", f);
            } catch(IOException ex){
                System.out.println(ex);
            } // End try-catch

            } // End actionPerformed
            }); // End ActionListener

        // Right Hand Side Panel
        rightHandSideP = new JPanel();
        rightHandSideP.setBackground(Color.DARK_GRAY);
        rightHandSideP.setPreferredSize(new Dimension(230,1000));
        this.add(rightHandSideP, BorderLayout.EAST);
        rightHandSideP.add(saveButton);
        saveButton.setPreferredSize(new Dimension(230,75));
        
        // Combo Box Panel
        comboBoxPanel = new JPanel(new GridLayout(5,1, GAP, GAP)); //Set layout and declared gap between buttons
        String comboBoxItems[] = {FilterPanel, DrawPanel, urlPanel}; // Name of drop down tabs
        JComboBox cb = new JComboBox(comboBoxItems); // Add items to JComboBox
        cb.setEditable(false);
        itemStateChanged item = new itemStateChanged(); 
        cb.addItemListener(item); // Change buttons displayed
        comboBoxPanel.add(cb); // Add to panel
       
        // New JPanel for first layer of buttons
        JPanel card1 = new JPanel();
        
        // Add filter buttons
        card1.add(grayScale);
        card1.add(inverted);
        card1.add(mirrored);
        card1.add(blur);
        card1.setBackground(Color.CYAN);
        
        // New JPanel for second layer containing the draw button
        JPanel card2 = new JPanel();
        // Add draw button
        card2.add(draw);
        card2.setBackground(Color.yellow);

        // New JPanel for third layer
        JPanel card3 = new JPanel();
        card3.setBackground(Color.red);
        card3.add(displayFile);

        // Set cardLayout and add to according panel
        cards = new JPanel(new CardLayout());
        cards.add(card1, FilterPanel);
        cards.add(card2, DrawPanel);
        cards.add(card3, urlPanel);

        // Add comboBoxPanel to left hand side
        this.add(comboBoxPanel, BorderLayout.WEST);
        this.add(cards, BorderLayout.NORTH); // Display Buttons on top
        comboBoxPanel.setBackground(Color.DARK_GRAY);
        comboBoxPanel.setPreferredSize(new Dimension(230,100));
        
        grayScale.setToolTipText("Click or press ALT-G");
        grayScale.setMnemonic('G'); //Hot key
        // ActionListener for grayscale button
        grayScale.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                   
            File f = selFile1;

            if(selFile1 == null) {
                JOptionPane.showMessageDialog(mirrored, "File is null, please upload a png file to apply filter.");
            return;
            }
            
            // Read file
            try{
                getImg = ImageIO.read(f);
            } catch(IOException ef){
                System.out.println(ef);
            } // End try-catch

            // Get file width and height
            int width = getImg.getWidth();
            int height = getImg.getHeight();

            // Convert to grayscale
            for(int y = 0; y < height; y++){
                for(int x = 0; x < width; x++){
                    int p = getImg.getRGB(x,y);

                    int a = (p>>24)&0xff;
                    int r = (p>>16)&0xff;
                    int g = (p>>8)&0xff;
                    int b = p&0xff;

                    // Calculate average
                    int avg = (r+g+b)/3;

                    // Replace RGB value with avg
                    p = (a<<24) | (avg<<16) | (avg<<8) | avg;

                    getImg.setRGB(x, y, p);
                }
            } // End for
            JOptionPane.showMessageDialog(comboBoxPanel, "Gray Scale Filter has been applied. Save to view!");
            // Write file
            try {
                
                f = output;
                ImageIO.write(getImg, "png", selFile1);
            } catch(IOException ex){
                System.out.println(ex);
            } // End try-catch
            
            } // End actionPerformed
        }); //End ActionListener

        inverted.setToolTipText("Click or press ALT-I");
        inverted.setMnemonic('I'); // Hot key
        // ActionListener for inverted
        inverted.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
            File f = selFile1;
            
            if(selFile1 == null) {
                JOptionPane.showMessageDialog(mirrored, "File is null, please upload a png file to apply filter.");
            return;
            }
            
            // Read file
            try{
                getImg = ImageIO.read(f);
            } catch(IOException eg){
                System.out.println(eg);
            } // End try-catch
            // Get image width and height
            int width = getImg.getWidth();
            int height = getImg.getHeight();
            // Convert to negative
            for(int y = 0; y < height; y++){
                for(int x = 0; x < width; x++){
                    int p = getImg.getRGB(x,y);
                    int a = (p>>24)&0xff;
                    int r = (p>>16)&0xff;
                    int g = (p>>8)&0xff;
                    int b = p&0xff;
                    // Subtract RGB from 255
                    r = 255 - r;
                    g = 255 - g;
                    b = 255 - b;
                    // Set new RGB value
                    p = (a<<24) | (r<<16) | (g<<8) | b;
                    getImg.setRGB(x, y, p);
                }
            }
            JOptionPane.showMessageDialog(comboBoxPanel, "Inverted Filter has been applied. Save to view!");
            // Write file
            try {
                f = output;
                System.out.println(f);
                ImageIO.write(getImg, "png", f);
            } catch(IOException ex){
                System.out.println(ex);
            } // End try-catch
        } // End actionPerformed       
        }); // End actionListener
   
        mirrored.setToolTipText("Click or press ALT-M");
        mirrored.setMnemonic('M'); // Hot Key
        // ActionListener for mirror button
        mirrored.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
           
                // If no file selected, display message
                if(selFile1 == null) {
                    JOptionPane.showMessageDialog(cards, "File is null, please upload a png file to apply filter.");
                    return;
                } else {
                
                // Warning message that file will be overwritten
                Component g = null;
                int n = JOptionPane.showOptionDialog(g, "Applying this filter will automatically save a new instance of the file to your desptop. Do you wish to continue?", "Check", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,null,null,null);
                switch(n) {
                case 0: // Yes
                    break;
                case 1: //No
                    System.exit(0);
                    break;
                case -1:
                    //User pressed 'X' button on the options dialog.If you want to exit app here.
                    System.exit(0);
                break;
                    } // End switch
                } // End if else
                
                // Read file
                try {
                    File f = new File(path1);

                    BufferedImage getImg = ImageIO.read(f);
                    // Get source image dimension
                    int width = getImg.getWidth();
                    int height = getImg.getHeight();
                    //BufferedImage for mirror image
                    BufferedImage mirroredImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
                    //create mirror image pixel by pixel
                    for (int y = 0; y < height; y++) {
                        for (int lx = 0, rx = width - 1; lx < width; lx++, rx--) {
                            // lx starts from the left side of the image
                            // rx starts from the right side of the image
                            // Get source pixel value
                            int p = getImg.getRGB(lx, y);
                            // Set mirror image pixel value - both left and right
                            mirroredImage.setRGB(rx, y, p);
                        }
                    } // End for
                    JOptionPane.showMessageDialog(comboBoxPanel, "Mirrored Filter has been applied. Check your desktop for an OutFile!");
                    // Write file
                    ImageIO.write(mirroredImage, "png", new File(path2));
                } catch (IOException ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(mirrored, "File failed to save, pls try again.");
                } // End try-catch
            } // End actionPerformed 
        }); // End actionListener
        
        blur.setToolTipText("Click or press ALT-S");
        blur.setMnemonic('B'); // Hot key
        // ActionListener for blur button
        blur.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // If no file selected, display message
                if(selFile1 == null) {
                    JOptionPane.showMessageDialog(cards, "File is null, please upload a png file to apply filter.");
                    return;
                } else {
                
                // Warning message that file will be overwritten
                Component g = null;
                int n = JOptionPane.showOptionDialog(g, "Applying and saving this filter will overwrite the image. Do you wish to continue?", "Check", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,null,null,null);
                switch(n) {
                case 0: // Yes
                    break;
                case 1: //No
                    System.exit(0);
                    break;
                case -1:
                    //User pressed 'X' button on the options dialog.If you want to exit app here.
                    System.exit(0);
                break;
                    } // End switch
                } // End if else
                
                // Reference methods inside private GaussianBlur
                GaussianBlur gb = new GaussianBlur();
                
                // Set and print matrix
                double[][] weights = gb.generateWeightMatrix(20, Math.sqrt(20));
                gb.printWeightedMatrixToFile(weights);
                
                // Read selFile1 and call create method
                BufferedImage answer = null;
                try {
                    BufferedImage source_image = ImageIO.read(selFile1);
                    answer = gb.createGaussianImage(source_image, weights, 20);
                    
                } catch (IOException ex) {
                    ex.printStackTrace();
                } // End try-catch

               JOptionPane.showMessageDialog(comboBoxPanel, "Blurred Filter has been applied. Save to view!");
                // Write file
                try {
                  
                    ImageIO.write(getImg, "png", new File (path1));
                } catch(IOException ex){
                    System.out.println(ex);
                } // End try-catch
            } // End actionPerformed
            
        }); // End ActionListener      
        
    } // End GUI_Image_Demo constructor

    // Private method that creates, generates, and prints weighted matrix for blue filter
    private class GaussianBlur {

    // Contructor
    GaussianBlur() {
    }

    // Build matrix, takes in x & y returns their weight
    public double gaussianModel(double x, double y, double variance) {
        return (1 / (2 * Math.PI * Math.pow(variance, 2))
                * Math.exp(-(Math.pow(x, 2) + Math.pow(y, 2)) / (2 * Math.pow(variance, 2))));
    } // End gaussianModel

    // Return multi dimensional array (2D array)
    // Larger radius increases blur since weighted average extended further from center pixel
    public double[][] generateWeightMatrix(int radius, double variance) {
        double[][] weights = new double[radius][radius];
        double summation = 0;
        for (int i = 0; i < weights.length; i++) {
            for (int j = 0; j < weights[i].length; j++) {
                weights[i][j] = gaussianModel(i - radius / 2, j - radius / 2, variance); // Assign to algo but 0,0 is top left, so subtract half radius to start in middle
                summation += weights[i][j]; // Keep track of summation to normalize data

                System.out.print(weights[i][j] + " ");
            }
            System.out.println();
        } // End for

        System.out.println("--------------------------------------------------");
        for (int i = 0; i < weights.length; i++) {
            for (int j = 0; j < weights[i].length; j++) {
                weights[i][j] /= summation; // Take each i,j element and divide/equal by summation so adds roughly to 1.0
            }
        } // End nested for
        System.out.println("Summation: " + summation);
        return weights;
    } // End generateWeightMatrix

    // Print out gaussian blur
    public void printWeightedMatrixToFile(double[][] weightMatrix) {

        BufferedImage img = new BufferedImage(weightMatrix.length, weightMatrix.length, BufferedImage.TYPE_INT_RGB);

        // Create maximum to know max out of whole 2D array, that becomes the white pixel
        double max = 0;
        for (int i = 0; i < weightMatrix.length; i++) {
            for (int j = 0; j < weightMatrix.length; j++) {
                max = Math.max(max, weightMatrix[i][j]);
            }
        } // End nested for
        for (int i = 0; i < weightMatrix.length; i++) {
            for (int j = 0; j < weightMatrix.length; j++) {
                int grayScaleValue = (int) ((weightMatrix[i][j] / max) * 255d); //Convert to value between 0 and 255
                img.setRGB(i, j, new Color(grayScaleValue, grayScaleValue, grayScaleValue).getRGB());
            }
        } // End nested for
        try {
            // Write out to file
            ImageIO.write(img, "png", new File ("gaussian.weights.png"));
        } //here
        catch (IOException ex) {
            ex.printStackTrace();
        } // End try-catch

    } // End printWeightedMatrixToFile

    // Take weight matrix and apply it to image
    public BufferedImage createGaussianImage(BufferedImage source_image, double weights[][], int radius) {
        getImg = new BufferedImage(source_image.getWidth(), source_image.getHeight(), BufferedImage.TYPE_INT_RGB);

        // Access each pixel data and its properties of image
        // Sub radius so not to go out of bounds (black edges)
        for (int x = 0; x < source_image.getWidth(); x++) {
            for (int y = 0; y < source_image.getHeight(); y++) {
                // Store RGB data for final answer to image
                double[][] distributedColorRed = new double[radius][radius];
                double[][] distributedColorGreen = new double[radius][radius];
                double[][] distributedColorBlue = new double[radius][radius];

                // Nested for loop allows to itterate around pixrl and get properties
                for (int weightX = 0; weightX < weights.length; weightX++) {
                    for (int weightY = 0; weightY < weights[weightX].length; weightY++) {
                            
                        
                            int sampleX = x + weightX - (weights.length / 2);
                            int sampleY = y + weightY - (weights.length / 2);
                            
                            // Get rid of black borders from bluring image
                            if(sampleX > source_image.getWidth()-1) {
                                int error_offset = sampleX = source_image.getWidth() -1;
                                sampleX = source_image.getWidth() - 1 - error_offset;
                            }
                            
                            if(sampleY > source_image.getHeight()-1) {
                                int error_offset = sampleY = source_image.getHeight() -1;
                                sampleY = source_image.getHeight() - 1 - error_offset; 
                            }
                            
                            // Check if step outside image in a negative direction
                            if(sampleX < 0) {
                                sampleX = Math.abs(sampleX);
                            }
                            
                            if(sampleY < 0) {
                                sampleY = Math.abs(sampleY);
                            }

                            double currentWeight = weights[weightX][weightY];
                            Color sampledColor = new Color(source_image.getRGB(sampleX, sampleY));

                            //Calc colors around center pixrl
                            distributedColorRed[weightX][weightY] = currentWeight * sampledColor.getRed();
                            distributedColorGreen[weightX][weightY] = currentWeight * sampledColor.getGreen();
                            distributedColorBlue[weightX][weightY] = currentWeight * sampledColor.getBlue();
             

                        }

                    } // End inner for loops

                    // Pass in parameters
                    getImg.setRGB(x, y, new Color(getWeightedColorValue(distributedColorRed), getWeightedColorValue(distributedColorGreen), getWeightedColorValue(distributedColorBlue)).getRGB());
                } 

            } // End out for loops

            return getImg;

        } // End CreateGaussiImage

    // Take in weighted colors
    // Contruct new color for final answer for all this data
    private int getWeightedColorValue(double[][] weightedColor) {
        double summation = 0;
        for (int i = 0; i < weightedColor.length; i++) {
            for (int j = 0; j < weightedColor[i].length; j++) {
                summation += weightedColor[i][j];
            }
        }
        return (int) summation;
    } // End getWeightedColorValue

    } // End public class GaussianBlur
    

    // Private method listening for switch of cards on drop down tab (comboBoxPanel)
    private class itemStateChanged extends JFrame implements ItemListener {
        itemStateChanged() {
            addItemListener(this);
        }
        
        @Override
        public void itemStateChanged(ItemEvent evt) {
            CardLayout c1 = (CardLayout) (cards.getLayout());
            c1.show(cards, (String)evt.getItem());
        } // End public itemStateChanged
    } // End private itemStateChanged

    // Private method listening for mouse drags to draw on JFrame
    private class MouseListener extends JFrame implements MouseMotionListener {
	GUI_Image_Demo guiImageDemo = new GUI_Image_Demo();
        // Create JFram for drawing, incorrect implementation 
	MouseListener(){
		
            addMouseMotionListener(this);

            setSize(400,400);
            setLayout(null);
            setVisible(true);
	} // End MouseListener 
        
        @Override
	public void mouseDragged(MouseEvent e) {
            Graphics g = getGraphics();
            g.setColor(Color.blue);
            g.fillOval(e.getX(), e.getY(), 20, 20);	
	} // End MouseDragged
	
        @Override
	public void mouseMoved(MouseEvent e){
	} // End mouseMoved	
    } // End private MouseListener
    
    // Resize Image to JFrame
    public ImageIcon resize(String imgPath) {
        ImageIcon path = new ImageIcon(imgPath);
        Image img = path.getImage();
        Image newImg = img.getScaledInstance(label.getWidth(), label.getHeight()
        , Image.SCALE_SMOOTH);
        ImageIcon image = new ImageIcon(newImg);
        return image;
    } // End resize
    
} // End public class GUI_Image_Demo
